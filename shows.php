<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <title>Sphereorigins | Shows</title>
    <link rel="shortcut icon" type="image/x-icon" href="dist/img/favicon.png" />
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/loader.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->
    <section id="shows" class="shows">

        <div class="shows-banner-section">
            <div class="banner">
                <img src="dist/img/show/show-banner.jpg" class="img-responsive center-block desktop visible-lg" alt="">
                <img src="dist/img/show/shows-tab.jpg" class="img-responsive center-block tablet visible-sm visible-md" alt="">
                <img src="dist/img/show/shows-mob.jpg" class="img-responsive center-block mob visible-xs" alt="">
            </div>
            <!-- <div class="container visible-lg">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="outer-valign">
                            <div class="inner-valign">
                                <div class="making-form-text hidden-xs hidden-sm" data-scroll>
                                    <img src="dist/img/show/show-text.png" class="" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>

        <div class="show-list-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Balika-Vadhu.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Balika Vadhu<br class="hidden-xs"></h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Peshwa-Bajirao.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Peshwa Bajirao</h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Internet-wala-Love.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Internet Wala Love</h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Gangaa.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Gangaa</h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Ek-tha-Raja-Ek-thi-Rani.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Ek Tha Raaja Ek Thi Raani<br class="hidden-xs"></h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Mere-Angane-Main.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Mere Angne Mein<br class="hidden-xs"></h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Piya-Rangrezz.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Piya Rangrezz</h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Saraswati-Chandra.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Saraswati Chandra</h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Silsila-S-02.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Silsila Badalte Rishton Ka - Season 2</h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Silsila-S-01.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Silsila Badalte Rishton Ka</h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Koi-Laut-Ke-Aaya-Hai.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Koi Laut Ke Aaya Hai</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="switch-to">
            <img src="dist/img/switch-to.png" class="img-responsive center-block" alt="">
        </div>

        <div class="our-works">
            <a class="work telefilms" href="telefilms.php">
                <div class="text">Telefilms</div>
            </a>
            <a class="work regional" href="regional.php">
                <div class="text">Regional</div>
            </a>
            <a class="work animation" href="animation.php">
                <div class="text">Animations</div>
            </a>
        </div>

    </section>
    <!--  end body content -->
    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>

</body>

</html>
