<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <title>Sphereorigins | News</title>
    <link rel="shortcut icon" type="image/x-icon" href="dist/img/favicon.png" />
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/loader.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->
    <section id="news" class="news">

        <div class="shows-banner-section">
            <div class="banner">
                <img src="dist/img/news/news-banner.jpg" class="img-responsive center-block desktop visible-lg" alt="">
                <img src="dist/img/news/news-tablet-banner.jpg" class="img-responsive center-block tablet visible-sm visible-md" alt="">
                <img src="dist/img/news/news-mobile-banner.jpg" class="img-responsive center-block mob visible-xs" alt="">
            </div>
        </div>

        <div class="news-list-section">
            <div class="container">
                <div class="select-row">
                    <div class="category-select">
                        <select class="wide">
                            <option>All</option>
                            <option value="1">Shows</option>
                            <option value="2">Telefilms</option>
                            <option value="3">Regional</option>
                            <option value="4">Animations</option>
                        </select>
                    </div>
                    <div class="year-select">
                        <select class="wide">
                            <option>Latest</option>
                            <option value="1">2019</option>
                            <option value="2">2018</option>
                            <option value="3">2016</option>
                            <option value="4">2015</option>
                        </select>
                    </div>
                </div>
                <div class="row-flex">
                    <div class="news-box news_gallery">
                        <div class="light-item" data-iframe="true" data-src="https://player.vimeo.com/video/115951816?color=03ffbc">
                            <a href="">
                                <img src="dist/img/news/dummy-img.png" class="img-responsive" alt="">
                                <div class="text">
                                    <p class="headline">Stats could hide more than they show' - Bhuvneshwar Kumar</p>
                                    <div><span class="read-more-btn">Read More</span></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="news-box">
                        <div>
                            <a href="https://www.espncricinfo.com/story/_/id/26538274/stats-define-everything-bhuvneshwar-kumar" target="_blank">
                                <img src="dist/img/news/dummy-img.png" class="img-responsive" alt="">
                                <div class="text">
                                    <p class="headline">Stats could hide more than they show' - Bhuvneshwar Kumar</p>
                                    <p class="sub-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    
                                    <div class="read-more-btn">Read More</div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="news-box news_gallery">
                        <div class="light-item" data-src="dist/img/news/dummy-img.png">
                            <a href="">
                                <img src="dist/img/news/dummy-img.png" class="img-responsive" alt="">
                                <div class="text">
                                    <p class="headline">Stats could hide more than they show' - Bhuvneshwar Kumar</p>
                                    <p class="sub-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <div class="read-more-btn">Read More</div>
                                </div>
                            </a>
                        </div>
                        <div class="hide light-item" data-src="dist/img/show/Amar-Babin.jpg"></div>
                        <div class="hide light-item" data-src="dist/img/show/Amar-Naam-Joyeta.jpg"></div>
                        <div class="hide light-item" data-src="dist/img/show/Balika-Vadhu.jpg"></div>
                        <div class="hide light-item" data-src="dist/img/show/Barasat.jpg"></div>
                    </div>
                    <div class="news-box">
                        <div>
                            <a href="https://www.espncricinfo.com/story/_/id/26538274/stats-define-everything-bhuvneshwar-kumar" target="_blank">
                                <img src="dist/img/news/dummy-img.png" class="img-responsive" alt="">
                                <div class="text">
                                    <p class="headline">Stats could hide more than they show' - Bhuvneshwar Kumar</p>
                                    <div class="read-more-btn">Read More</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--  end body content -->
    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>
    <script>
      $(document).ready(function() {
        $('.news_gallery').lightGallery({
            thumbnail: false,
            share: false,
            fullScreen: false,
            download: false,
            autoplayControls: false,
            zoom: false,
            iframeMaxWidth: '80%',
            selector:'.light-item'
        });
    });
    </script>

</body>

</html>
