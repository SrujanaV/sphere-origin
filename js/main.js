var winH = $(window).outerHeight();

// homepage slider starts

$('#bootstrap-touch-slider').bsTouchSlider();

// homepage slider ends

// indian entertainment section slider starts

$(document).ready(function(){
    $('.indian-entertaiment-slider').owlCarousel({
        margin: 30,
        autoplay: true,
        dots: false,
        navText: false,
        // navText: ["<img src='./frontend/dist/img/arrow.png'>", "<img src='./frontend/dist/img/arrow.png'>"],
        autoplayTimeout: 4000,
        responsiveClass: true,
        responsive: {
            0: {
                center: true,
                items: 2,
                nav: false,
                
            },
            600: {
                center: true,
                items: 2,
                nav: false,
                
            },
            768: {
                center: true,
                items: 2
            },
            1000: {
                items: 3,
                nav: true
            }
        }
    });

    $('.achievements-slider').owlCarousel({
        loop: true,
        autoplay: true,
        margin:10,
        dots: true,
        navText: false,
        autoplayTimeout: 6000,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            768: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

    $('.quotes-slider').owlCarousel({
        loop: true,
        autoplay: true,
        dots: false,
        navText: false,
        autoplayTimeout: 4000,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            768: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
});

// indian entertainment section slider ends

// scrollout initialise starts

ScrollOut({
    /* options */
    // offset: 400

});

// scrollout initialise ends

// show page show-text animation starts

// $(window).load(function () {
//     $('.shows-banner-section').find('.making-form-text').css('width', '100%');
//     $('.telefilms-banner-section').find('.making-form-text').css('width', '100%');
//     $('.regional-banner-section').find('.making-form-text').css('width', '100%');
//     $('.hopmotion-animations-banner-section').find('.making-form-text').css('width', '100%');
// });


$(window).on('load', function () {
    setTimeout(function () {
        $('.loader_overlay').fadeOut(500);
    },0);
});

// show page show-text animation ends

// blog inside slider section slider starts

$('.blog-inside-slider').owlCarousel({
    loop: true,
    margin: 0,
    autoplay: true,
    dots: false,
    nav: true,
    navText: false,
    // navText: ["<img src='dist/img/arrow.png'>", "<img src='dist/img/arrow.png'>"],
    autoplayTimeout: 4000,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
            dots: true
        },
        600: {
            items: 1,
            nav: false,
            dots: true
        },
        1000: {
            items: 1,
            nav: true
        }
    }
})

// blog inside slider section slider ends

// // show-inside page slider starts

// $(document).ready(function(){
//     $('.show-inside-behind-the-scene').owlCarousel({
//         stagePadding: 0,
//         loop: true,
//         margin: 50,
//         autoplay: true,
//         dots: false,
//         nav: true,
//         navText: false,
//         // navText: ["<img src='dist/img/show-inside/white-arrow.png'>", "<img src='dist/img/show-inside/white-arrow.png'>"],
//         autoplayTimeout: 4000,
//         responsiveClass: true,
//         responsive: {
//             0: {
//                 center: true,
//                 items: 2,
//                 margin: 10,
//                 nav: false,
//             },
//             600: {
//                 center: true,
//                 items: 2,
//                 margin: 10,
//                 nav: false,
//             },
//             1000: {
//                 items: 1.7
//             }
//         }
//     })
// });


// // show-inside page slider ends




//========toggle sidebar============
$('.nav-icon3').click(function () {
    $(this).toggleClass('open');
    $('.sidebar').toggleClass('sidebar_toggle');
});

$('.menu_toggle_icon').on('click', function () {
    $(this).toggleClass('invert');
    var getParent = $(this).parents('.dropmenu');
    $(getParent).find('.droplist').slideToggle();
});

// Slider Tab Mobile Starts Here
$(document).ready(function () {
    $('#select-tab-mob').change(function () {
        var getTabVal = $(this).val();
        console.log(getTabVal);
        $(".entertainment-slider .nav-tabs a[href = " + getTabVal + "]").trigger('click');
        // $('.entertainment-slider .nav-tabs li:nth-child(3) a').trigger('click');
    });
});
// Slider Tab Mobile Ends Here



$.validator.addMethod("lettersonly", function (value, element) {
    return this.optional(element) || /^[a-zA-Z][a-zA-Z ]+$/i.test(value);
});

//for email only
$.validator.addMethod("emailtest", function (value, element) {
    return this.optional(element) || /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/i.test(value);
});



$("#auditionsForm").validate({
 rules: {
    first_name: {
        required: true,
        minlength: 2,
        lettersonly: true
    },
    last_name: {
        required: true,
        minlength: 2,
        lettersonly: true
    },
    email: {
        required: true,
        email: true
    },
    phone_no: {
        required: true,
        minlength: 10,
        maxlength: 10,
        digits: true
    },
    file1: {
        required: true
    },
    file2: {
        required: true
    },
    comment: {
        required: true
    }
},
messages: {
    first_name: {
        required: 'Enter first name',
        minlength: 'Enter atleast two character',
        lettersonly: 'Must enter character only'
    },
    last_name: {
        required: 'Enter last name',
        minlength: 'Enter atleast two character',
        lettersonly: 'Must enter character only'
    },
    email: {
        required: 'Enter email address',
        emailtest: " Please enter a valid email address"
    },
    phone_no: {
        required: 'Enter mobile number',
        maxlength: 'Enter 10 digits only',
        digits: 'Must enter number only'
    },
    file1: {
        required: 'Choose a file'
    },
    file2: {
        required: 'Choose a file'
    },
    comment: {
        required: 'Enter comment here'
    }
}
});

$("#contactForm").validate({
 rules: {
    name: {
        required: true,
        minlength: 2,
        lettersonly: true
    },
    email: {
        required: true,
        email: true
    },
    phone_no: {
        required: true,
        minlength: 10,
        maxlength: 10,
        digits: true
    },
    subject: {
        required: true
    },
    msg: {
        required: true
    }
},
messages: {
    name: {
        required: 'Enter your name',
        minlength: 'Enter atleast two character',
        lettersonly: 'Must enter character only'
    },
    
    email: {
        required: 'Enter email address',
        emailtest: " Please enter a valid email address"
    },
    phone_no: {
        required: 'enter mobile number',
        maxlength: 'Enter 10 digits only',
        digits: 'Must enter number only'
    },
    subject: {
        required: 'Enter subject'
    },
    msg: {
        required: 'This field is required'
    }
}
});

// $(document).ready(function() {
//     $('select').niceSelect();
// });

// $(function() {
//     // Generic selector to be used anywhere
//     $(".scroll-div").click(function(e) {
//         // Get the href dynamically
//         var destination = $(this).attr('href');
//         // Prevent href=“#” link from changing the URL hash (optional)
//         e.preventDefault();
//         // Animate scroll to destination
//         $('html, body').animate({
//         scrollTop: $(destination).offset().top
//         }, 1500);
//     });
// });

