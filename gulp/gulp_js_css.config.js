function jscss() {


    var pathconfig = {

        css_path: [
			'./css/bootstrap.css',
            './css/bootstrap-theme.css',
            './css/animate.css',
            './css/bootstrap-touch-slider.css',
            './css/font-awesome.min.css',
            './css/owl.theme.default.css',
			'./css/owl.carousel.min.css',
			'./css/nice-select.css',
			'./css/style.css'
		],

        js_path: [
			'./js/vendor/jquery-1.11.2.min.js',
            './js/vendor/bootstrap.js',
            './js/vendor/bootstrap-touch-slider-min.js',
            './js/vendor/owl.carousel.min.js',
			'./js/vendor/scroll-out.min.js',
            './js/vendor/jquery.validate.min.js',
            './js/vendor/jquery.nice-select.min.js',
			// Material Files

			'./js/vendor/materialize_js/velocity.min.js',
            './js/vendor/materialize_js/global.js',
            // './js/vendor/materialize_js/collapsible.js',
            // './js/vendor/materialize_js/dropdown.js',
            // './js/vendor/materialize_js/modal.js',
            // './js/vendor/materialize_js/materialbox.js',
            // './js/vendor/materialize_js/parallax.js',
            // './js/vendor/materialize_js/tabs.js',
            // './js/vendor/materialize_js/tooltip.js',
            // './js/vendor/materialize_js/waves.js',
            // './js/vendor/materialize_js/toasts.js',
            // './js/vendor/materialize_js/sidenav.js',
            // './js/vendor/materialize_js/scrollspy.js',
            // './js/vendor/materialize_js/autocomplete.js',
            './js/vendor/materialize_js/forms.js',
            // './js/vendor/materialize_js/slider.js',
            // './js/vendor/materialize_js/cards.js',
            // './js/vendor/materialize_js/chips.js',
            // './js/vendor/materialize_js/pushpin.js',
            // './js/vendor/materialize_js/buttons.js',
            // './js/vendor/materialize_js/datepicker.js',
            // './js/vendor/materialize_js/timepicker.js',
            // './js/vendor/materialize_js/characterCounter.js',
            // './js/vendor/materialize_js/carousel.js',
            // './js/vendor/materialize_js/tapTarget.js',
            // './js/vendor/materialize_js/select.js',
            // './js/vendor/materialize_js/range.js',

			// end
			'./js/main.js'
		]
    }

    return pathconfig;

}

module.exports = jscss();






/*src: [
    "velocity.min.js",
    "global.js",
    "collapsible.js",
    "dropdown.js",
    "modal.js",
    "materialbox.js",
    "parallax.js",
    "tabs.js",
    "tooltip.js",
    "waves.js",
    "toasts.js",
    "sidenav.js",
    "scrollspy.js",
    "autocomplete.js",
    "forms.js",
    "slider.js",
    "cards.js",
    "chips.js",
    "pushpin.js",
    "buttons.js",
    "datepicker.js",
    "timepicker.js",
    "characterCounter.js",
    "carousel.js",
    "tapTarget.js",
    "select.js",
    "range.js",
]*/
