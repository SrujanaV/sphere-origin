<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <title>Sphereorigins | Contact</title>
    <link rel="shortcut icon" type="image/x-icon" href="dist/img/favicon.png" />
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/loader.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->
    <section id="contact" class="contact_wrap">

        <div class="container-fluid">
            <div class="row">
                <div id="banner">
                    <img src="dist/img/contact/contact-us-banner.jpg" class="img-responsive">
                    <!-- <h2 class="heading">Contact us</h2> -->
                </div>


                <div class="container">
                    <div class="row formWrap">
                        <img src="dist/img/contact/start-pattern1.png" class="img-responsive left-pattern">
                        <div class="col-lg-5 address">
                            <div class="row">
                                <div class="addressWrap">
                                    <img src="dist/img/contact/box.png" class="img-responsive">
                                    <div class="address-container">
                                        <h3 class="text-left">Registered Office</h3>
                                        <div class="desc">
                                            <p><span class="fas fa-map-marker-alt"></span></p>
                                            <p>Sphereorigins Multivision Private Limited<br />
                                                401, Nanak Chambers,<br />
                                                Opposite Fun Republic,<br />
                                                Off New Link Road,<br />
                                                Andheri (West),<br />
                                                Mumbai – 400053, India</p>
                                        </div>

                                        <div class="desc">
                                            <p><span class="fas fa-phone"></span></p>
                                            <p>Tel: <a href="tel:02240532000">+91 22 4053 2000</a></p>
                                        </div>

                                        <div class="desc">

                                            <p><span class="fas fa-envelope"></span></p>
                                            <p><a href="mailto:info@sphereorigins.com">info@sphereorigins.com</a></p>
                                        </div>
                                    </div>
                                    <!-- <h4 class="text-center map-direction"><a>GET DIRECTIONS ON GOOGLE MAPS</a></h4> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="row form-container">
                                <form id="contactForm" method="post">
                                    <div class="input-field col-xs-12 col-lg-6">
                                        <input id="name" name="name" type="text" class="validate">
                                        <label for="name">Name<em>*</em></label>
                                    </div>
                                    <div class="input-field col-xs-12 col-lg-6">
                                        <input id="phone_no" name="phone_no" type="text" class="validate">
                                        <label for="phone_no">Phone no<em>*</em></label>
                                    </div>
                                    <div class="input-field col-xs-12 col-lg-6">
                                        <input id="email" name="email" type="email" class="validate">
                                        <label for="email">Email Address<em>*</em></label>
                                    </div>
                                    <div class="input-field col-xs-12 col-lg-6">
                                        <input id="subject" name="subject" type="text" class="validate">
                                        <label for="subject">subject<em>*</em></label>
                                    </div>
                                    <div class="input-field col-xs-12">
                                        <input id="msg" name="msg" type="text" class="validate">
                                        <label for="msg">Message<em>*</em></label>
                                    </div>
                                    <!-- <div class="input-field col-xs-12">
                                        <p>Captcha<em>*</em></p>
                                        <p>Enter code shown in below image.</p>
                                        <div class="captcha_code">
                                            <span class="code">93Ksb035</span>
                                            <span><img src="dist/img/contact/reload.png" class=""></span>
                                            <span> Not readable? Change text </span>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="row">
                                                <input type="text" name="captcha" class="captchabox" placeholder="enter code">
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="col-xs-12">
                                        <button type="submit" value="submit" class="btn submit-btn">submit</button>
                                    </div>
                                </form>
                                <img src="dist/img/contact/start-pattern2.png" class="img-responsive right-pattern">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mapWrap">
                    <!--<img src="dist/img/contact/map.png" class="img-responsive">-->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15077.471576607199!2d72.8327465!3d19.1353669!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf1aa8bae1e1fe8bd!2sSphereorigins%20Multivision%20Private%20Limited!5e0!3m2!1sen!2sin!4v1568110190241!5m2!1sen!2sin" width="100%" height="664" frameborder="0" style="border:0;" allowfullscreen="" class="map"></iframe>
                </div>

                <div class="container">
                    <div class="contact-yellow-bg-enquires up" data-scroll>
                        <div class="for-box">
                            <div class="for-content">
                                <div class="image-icon"><img src="dist/img/general-queries.png" class="img-responsive" alt=""></div>
                                <p>For any general queries email us on <br/> <a href="mailto:info@sphereorigins.com">info@sphereorigins.com</a></p>
                            </div>
                        </div>
                        <div class="for-box">
                            <div class="for-content">
                                <div class="image-icon"><img src="dist/img/casting-icon.png" class="img-responsive" alt=""></div>
                                <p>For casting related queries email us on <br/> <a href="mailto:casting@sphereorigins.com">casting@sphereorigins.com</a></p>
                            </div>
                            
                        </div>
                        <div class="for-box">
                            <div class="for-content">
                                <div class="image-icon"><img src="dist/img/animation-icon.png" class="img-responsive" alt=""></div>
                                <p>For animation related queries email us on <br/> <a href="mailto:info@hopmotion.com">info@hopmotion.com</a></p>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row contactDetails">
                        <div class="col-xs-12 col-sm-6 col-md-3 details">
                            <p>For info related queries email us <br />
                                on <a href="mailto:info@sphereorigins.com">info@sphereorigins.com</a> </p>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 details">
                            <p>For creative related queries email us
                                <br />
                                on <a href="mailto:creative@sphereorigins.com">creative@sphereorigins.com</a></p>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 details">
                            <p>For marketing related queries email us <br />
                                on <a href="mailto:marketing@sphereorigins.com">marketing@sphereorigins.com</a></p>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 details">
                            <p>For syndication related queries email us <br />
                                on <a href="mailto:syndication@sphereorigins.com">syndication@sphereorigins.com</a></p>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>

    </section>
    <!--  end body content -->
    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>

</body>

</html>
