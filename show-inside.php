<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->

<head>
    <link rel="shortcut icon" type="image/x-icon" href="dist/img/favicon.png" />
    <?php include("includes/include_css.html") ?>
</head>

<body>
<?php include("includes/browser_upgrade.html") ?>
<?php include("includes/loader.html") ?>
<?php include("includes/header.html") ?>
<?php include("includes/sidebar.html") ?>
<!--  insert body content  -->
<section id="show-inside" class="show-inside">

    <div class="show-inside-bg">
        <div class="container yellow-bg">
            <div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 show-inside-desc">
                        <img src="dist/img/show-inside/show-inside.png" alt="" class="img-responsive main-show-inside-img">
                        <div class="row-flex-trailer">
                            <div class="title-serial">
                                <h1>PESHWA BAJIRAO:</h1>
                                <h2><span class="channel">SONY</span> 7.30 p.m. Monday to Friday</h2>
                            </div>
                            <div class="trailer-btn" id="video-gallery">
                                <div class="light-item" data-src="https://www.youtube.com/watch?v=SH2BiZFwn9o">                                    
                                    <img src="dist/img/youtube.png" class="img-responsive" alt="">
                                    <div class="text">Watch Trailer</div>
                                </div>
                            </div>
                        </div>              
                        
                    </div>
                </div>
                <div class="row inside-the-text-section">
                    <div class="col-md-5">
                        <h3 class="">
                            <div class="making-form-text" data-scroll>
                                <img src="dist/img/show-inside/inside-the-text.png" class="inside-the-img" alt="">
                            </div>
                            <div class="down story-text" data-scroll>Story</div>
                        </h3>
                        <img src="dist/img/show-inside/zig-zag.png" alt="" class="img-responsive zig-zag-inside-the-story">
                    </div>
                    <div class="col-md-7">
                        <p>Behind every extraordinary human being, there are valuable life lessons unseen. Some learn these lessons through life and others have exemplary teachers, like Bajirao had; his parents, Balaji Vishwanath and Radhabai, who despite having to scamper for their life, stood strongly for their values, tradition and what is right.</p>
                        <p>This leaf out of history is of paramount importance to contemporary India, particularly when our nation is in search of thought leaders who can galvanize the nation to achieve the impossible. Peshwa is the journey of a warrior who fought and loved with equal passion, one, who stood up for what he believed in, and achieved in one lifetime more than most men can even think of, because true greatness is achieved only by those who are ready to challenge stereotypes, push themselves beyond their limits and overcome all their fears.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="three-section-bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-11 col-md-offset-1 no-padd">
                    <ul class="nav nav-tabs">
                        <li class="active">
                          <a href="#tab1" data-toggle="tab">Behind the scenes</a>
                        </li>
                        <li>
                          <a href="#tab2" data-toggle="tab">Interview with stars</a>
                        </li>
                     </ul>
                     <div class="tab-content up" data-scroll="in">
                        <div class="tab-pane active" id="tab1">
                            <div class="owl-carousel owl-theme show-inside-behind-the-scene">
                                <div class="item">
                                    <img src="dist/img/show-inside/behind1.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="dist/img/show-inside/behind2.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="dist/img/show-inside/behind1.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="dist/img/show-inside/behind2.jpg" alt="" class="img-responsive">
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                            <div class="owl-carousel owl-theme show-inside-behind-the-scene">
                                <div class="item">
                                    <img src="dist/img/show-inside/behind1.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="dist/img/show-inside/behind2.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="dist/img/show-inside/behind1.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="dist/img/show-inside/behind2.jpg" alt="" class="img-responsive">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid character-sketch-section">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="making-form-text" data-scroll>
                        <img src="dist/img/show-inside/actors.png" class="character-text" alt="">
                    </div>
                    <img src="dist/img/show-inside/speak.png" alt="" class="img-responsive sketch-img img-mid down" data-scroll>
                    <img src="dist/img/show-inside/grey-zig-zag.png" alt="" class="img-responsive img-mid appear" data-scroll>
                </div>
                    
                <div class="col-md-10 col-md-offset-1 character-sketch-yellow-bg">
                    <!-- <div id="video-gallery">
                        <a class="col-md-4 col-sm-4 character-sketch-blocks" href="https://vimeo.com/132200060">
                            <img src="dist/img/show-inside/character1.png" alt="" class="img-responsive">
                            <img src="dist/img/youtube.png" alt="" class="img-responsive play-btn">
                            <h3>Hiten Tejwani</h3>
                        </a>
                        <a class="col-md-4  col-sm-4 character-sketch-blocks" href="https://vimeo.com/250435533">
                            <img src="dist/img/show-inside/character1.png" alt="" class="img-responsive">
                            <img src="dist/img/youtube.png" alt="" class="img-responsive play-btn">
                            <h3>Harsh Rajput</h3>
                        </a>
                        <a class="col-md-4 col-sm-4 character-sketch-blocks" href="https://vimeo.com/142059045">
                            <img src="dist/img/show-inside/character1.png" alt="" class="img-responsive">
                            <img src="dist/img/youtube.png" alt="" class="img-responsive play-btn">
                            <h3>Hiba Nawab</h3>
                        </a>                   
                    </div> -->
                    <div id="video-gallery">
                        <a class="col-md-4 col-sm-4 character-sketch-blocks" data-iframe="true" data-src="https://player.vimeo.com/video/115951816?color=03ffbc">
                            <img src="dist/img/show-inside/character1.png" alt="" class="img-responsive">
                            <img src="dist/img/youtube.png" alt="" class="img-responsive play-btn">
                            <h3>Hiten Tejwani</h3>
                        </a>
                        <a class="col-md-4  col-sm-4 character-sketch-blocks" data-iframe="true" data-src="https://player.vimeo.com/video/131639123?title=0&byline=0&portrait=0">
                            <img src="dist/img/show-inside/character1.png" alt="" class="img-responsive">
                            <img src="dist/img/youtube.png" alt="" class="img-responsive play-btn">
                            <h3>Harsh Rajput</h3>
                        </a>
                        <a class="col-md-4 col-sm-4 character-sketch-blocks" data-iframe="true" data-src="https://player.vimeo.com/video/74799437">
                            <img src="dist/img/show-inside/character1.png" alt="" class="img-responsive">
                            <img src="dist/img/youtube.png" alt="" class="img-responsive play-btn">
                            <h3>Hiba Nawab</h3>
                        </a>                   
                    </div>
                </div>

            </div>
        </div>
        <div class="container related-shows-section">
            <div class="row">
                <div class="col-md-12 related-shows-head">
                    <h4>Related Shows</h4>
                </div>
                <div class="col-md-4 col-sm-4 show-list-desc">
                    <img src="dist/img/show/Silsila-S-02.jpg" alt="" class="img-responsive">
                    <h2>Silsila Badallte<br class="hidden-xs">Rishton Ka - Season 2</h2>
                </div>
                <div class="col-md-4 col-sm-4 show-list-desc">
                    <img src="dist/img/show/Koi-Laut-Ke-Aaya-Hai.jpg" alt="" class="img-responsive">
                    <h2>Koi Laut Ke Aaya Hai</h2>
                </div>
                <div class="col-md-4 col-sm-4 show-list-desc">
                    <img src="dist/img/show/Silsila-S-01.jpg" alt="" class="img-responsive">
                    <h2>Silsila Badallte<br class="hidden-xs">Rishton Ka</h2>
                </div>
            </div>
        </div>
    </div>

</section>
<!--  end body content -->
<?php include("includes/footer.html") ?>
<?php include("includes/include_js.html") ?>

<script>
    $(document).ready(function() {
        $('#video-gallery').lightGallery({
            thumbnail: false,
            share: false,
            fullScreen: false,
            download: false,
            autoplayControls: false,
            zoom: false
        });
    });

    // show-inside page slider starts

$(document).ready(function(){
    $('.show-inside-behind-the-scene').owlCarousel({
        stagePadding: 0,
        loop: true,
        margin: 50,
        autoplay: true,
        dots: false,
        nav: true,
        navText: false,
        autoplayTimeout: 4000,
        responsiveClass: true,
        responsive: {
            0: {
                center: true,
                items: 2,
                margin: 10,
                nav: false,
            },
            600: {
                center: true,
                items: 2,
                margin: 10,
                nav: false,
            },
            1000: {
                items: 1.7
            }
        }
    })
});


// show-inside page slider ends
</script>

</body>
</html>
