<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <title>Sphereorigins | Hopmotion Animation</title>
    <link rel="shortcut icon" type="image/x-icon" href="dist/img/hop-favicon.png" />
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <?php include("includes/browser_upgrade.html") ?>

    <?php include("includes/loader.html") ?>
    <?php include("includes/hopemotion-header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->
    <section id="hopemotion" class="hopemotion">

        <div id="bootstrap-touch-slider" class="carousel bs-slider fade  control-round indicators-line hide" data-ride="carousel" data-pause="hover" data-interval="false">

            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#bootstrap-touch-slider" data-slide-to="0" class="active"></li>
                <li data-target="#bootstrap-touch-slider" data-slide-to="1"></li>
                <li data-target="#bootstrap-touch-slider" data-slide-to="2"></li>
                <li data-target="#bootstrap-touch-slider" data-slide-to="3"></li>
                <li data-target="#bootstrap-touch-slider" data-slide-to="4"></li>
            </ol>

            <!-- Wrapper For Slides -->
            <div class="carousel-inner" role="listbox">

                <!-- One Slide -->
                <div class="item active">

                    <!-- Slide Background -->
                    <img src="dist/img/hopemotion/banner-1.jpg" alt="" class="slide-image" />
                    <!-- <div class="bs-slider-overlay"></div> -->

                    <div class="container">
                        <div class="row">
                            <!-- Slide Text Layer -->
                            <div class="slide-text slide_style_center banner-slider">
                                <h1 data-animation="animated flipInX">Animation that</h1>
                                <p data-animation="animated lightSpeedIn">is Indian in origin, global in quality</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->

                <!-- Second Slide -->
                <div class="item">

                    <!-- Slide Background -->
                    <img src="dist/img/hopemotion/banner-2.jpg" alt="" class="slide-image" />
                    <!-- <div class="bs-slider-overlay"></div> -->

                    <div class="container">
                        <div class="row">
                            <!-- Slide Text Layer -->
                            <div class="slide-text slide_style_center banner-slider">
                                <h1 data-animation="animated flipInX">Animation that</h1>
                                <p data-animation="animated lightSpeedIn">brings stories to life</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->

                <!-- Third Slide -->
                <div class="item">

                    <!-- Slide Background -->
                    <img src="dist/img/hopemotion/banner-3.jpg" alt="" class="slide-image" />
                    <!-- <div class="bs-slider-overlay"></div> -->

                    <div class="container">
                        <div class="row">
                            <!-- Slide Text Layer -->
                            <div class="slide-text slide_style_center banner-slider">
                                <h1 data-animation="animated flipInX">Animation that</h1>
                                <p data-animation="animated lightSpeedIn">creates memorable characters</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->

                <!-- Fourth Slide -->
                <div class="item">

                    <!-- Slide Background -->
                    <img src="dist/img/hopemotion/banner-4.jpg" alt="" class="slide-image" />
                    <!-- <div class="bs-slider-overlay"></div> -->

                    <div class="container">
                        <div class="row">
                            <!-- Slide Text Layer -->
                            <div class="slide-text slide_style_center banner-slider">
                                <h1 data-animation="animated flipInX">Animation that</h1>
                                <p data-animation="animated lightSpeedIn">motivates children to think</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->

                <!-- Fifth Slide -->
                <div class="item">

                    <!-- Slide Background -->
                    <img src="dist/img/hopemotion/banner-5.jpg" alt="" class="slide-image" />
                    <!-- <div class="bs-slider-overlay"></div> -->

                    <div class="container">
                        <div class="row">
                            <!-- Slide Text Layer -->
                            <div class="slide-text slide_style_center banner-slider">
                                <h1 data-animation="animated flipInX">Animation that</h1>
                                <p data-animation="animated lightSpeedIn">sparks imagination</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Slide -->

            </div><!-- End of Wrapper For Slides -->

            <!-- Left Control -->
            <a class="left carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="prev">
                <span class="fa fa-angle-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <!-- Right Control -->
            <a class="right carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="next">
                <span class="fa fa-angle-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div> <!-- End  bootstrap-touch-slider Slider -->

        <div class="video-banner">
            <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop" class="hidden-xs">
                <source src="dist/img/hopemotion/hopmotion-desktop.mp4" type="video/mp4">
            </video>
            <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop" class="visible-xs">
                <source src="dist/img/hopemotion/hopmotion-mobile.mp4" type="video/mp4">
            </video>
            <div class="scroll-div" href=".go-to-scroll">
                <p><span></span>Scroll Down</p>
            </div>
        </div>

        <div class="technology-section go-to-scroll">
            <div class="dots-bg"><img src="dist/img/hopemotion/technology-bg-dots.png" class="img-responsive" alt=""></div>
            <div class="sky-bg"><img src="dist/img/hopemotion/blue-sky.png" class="img-responsive" alt=""></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="innovation-text">
                            <div class="making-form-text" data-scroll>
                                <img src="dist/img/hopemotion/the-innovative.png" class="making-text" alt="">
                            </div>
                            <img src="dist/img/hopemotion/innovative-animation-studio.png" alt="" class="img-responsive down yellow-img-font-mob" data-scroll>
                            <img src="dist/img/zig-zag.png" alt="" class="img-responsive appear" data-scroll> <br/>
                            <p>An animation studio that does it all — from conceptual drawing to final rendered images — Hopmotion is a team of creative minds with a passion for art and technology. Our focus is to produce broadcast-quality animated content for television and digital, in both domestic and international markets. The latest technologies are at the heart of what we do at Hopmotion Animation; it is how we keep up with the growing demands of the entertainment business.</p> <br/>
                            <p>Ours is an animation studio that innovates to entertain.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 ipad-text-center">
                        <div class="technology-text">
                            <div class="making-form-text" data-scroll>
                                <img src="dist/img/hopemotion/technology.png" class="making-text" alt="">
                            </div>
                            <img src="dist/img/hopemotion/as-advanced.png" alt="" class="img-responsive down yellow-img-font-mob " data-scroll>
                            <img src="dist/img/zig-zag.png" alt="" class="img-responsive appear" data-scroll>
                            <div class="hippo"><img src="dist/img/hopemotion/hippo.png" class="img-responsive" alt=""></div>
                        </div>
                    </div>
                </div>

                <div class="row technology-grey-bg" data-scroll>
                    <div class="col-md-4 col-sm-4">
                        <div class="technology-block">
                            <h2>State-of-the-Art <br /> Tech Facilities</h2>
                            <p>Our world-class infrastructure has made it possible for us to set records and win awards, including the 2016 FICCI BAF Award for Chhoti Anandi.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="technology-block">
                            <h2>Studios in Mumbai <br /> & Hyderabad</h2>
                            <p>One houses senior writers and creative talent; the other seats over a hundred artists.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="technology-block">
                            <h2>Toon Boom <br /> Harmony Connection</h2>
                            <p>A connection that has made our animation smoother and more time-efficient.</p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="three-wheeler"><img src="dist/img/hopemotion/three-wheeler.png" class="img-responsive" alt=""></div>
            <div class="anandi-friends"><img src="dist/img/hopemotion/choti-anandi-friends.png" class="img-responsive" alt=""></div>
        </div>

        <div class="fondest-section">
            <div class="dots-bg"><img src="dist/img/hopemotion/middle-white-dots.png" class="img-responsive" alt=""></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ipad-text-center">
                        <div class="technology-text">
                            <div class="hippo"><img src="dist/img/hopemotion/hippo.png" class="img-responsive" alt=""></div>
                            <div class="making-form-text" data-scroll>
                                <img src="dist/img/hopemotion/indias-fondest.png" class="making-text" alt="">
                            </div>
                            <img src="dist/img/hopemotion/animated-wonders.png" alt="" class="img-responsive down yellow-img-font-mob " data-scroll>
                            <img src="dist/img/zig-zag.png" alt="" class="img-responsive appear" data-scroll>
                        </div>
                    </div>
                </div>


                <div class="series-row one">
                    <div class="img-series">
                        <div class="anandi-bg"><img src="dist/img/hopemotion/choti-anandi-bg.png" class="img-responsive" alt=""></div>
                        <div class="character-img anandi"><img src="dist/img/hopemotion/choti-anandi.png" class="img-responsive" alt=""></div>
                    </div>
                    <div class="text-series">
                        <div class="series-box">
                            <div class="title down" data-scroll>Chhoti <br /> Anandi <img src="dist/img/zig-zag.png" alt="" class="img-responsive appear" data-scroll></div>
                            <div class="brief">The much-loved animated spin-off of the acclaimed Balika Vadhu</div>
                        </div>

                    </div>
                </div>

                <div class="series-row two">
                    <div class="text-series">
                        <div class="series-box">
                            <div class="brief">The adventures of an adorable octogenarian who has been hogging Nickelodeon India's air time</div>
                            <div class="title down" data-scroll>Daaduji<img src="dist/img/zig-zag.png" alt="" class="img-responsive appear" data-scroll></div>
                        </div>

                    </div>
                    <div class="img-series">
                        <div class="anandi-bg"><img src="dist/img/hopemotion/daduji-bg.png" class="img-responsive" alt=""></div>
                        <div class="character-img daduji"><img src="dist/img/hopemotion/daduji.png" class="img-responsive" alt=""></div>
                    </div>
                </div>

                <div class="series-row three">
                    <div class="img-series">
                        <div class="anandi-bg"><img src="dist/img/hopemotion/kuku-goat-bg.png" class="img-responsive" alt=""></div>
                        <div class="character-img kuku-goat"><img src="dist/img/hopemotion/kuku-goat.png" class="img-responsive" alt=""></div>
                    </div>
                    <div class="text-series">
                        <div class="series-box">
                            <div class="title down" data-scroll>Kuku <br /> Mey Mey <img src="dist/img/zig-zag.png" alt="" class="img-responsive appear" data-scroll></div>
                            <div class="brief">
                                A hilarious FICCI-nominated laugh about a docile lion and a clever goat.
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="tree"><img src="dist/img/hopemotion/tree.png" class="img-responsive" alt=""></div>
        <div class="snail"><img src="dist/img/hopemotion/snail.png" class="img-responsive" alt=""></div>

        <div class="live-animation-event">
            <img src="dist/img/career/live-animation-event.jpg" class="img-responsive">
        </div>

        <section class="people">
            <div class="meet-our-people">
                <div class="live-animation-event">
                    <h2>ALL SMILES AT ORBIT LIVE ANIMATION EVENT – HOPMOTION WINS INDUSTRY HONORS 2017</h2>
                    <p>Sphereorigins has proved its excellence by winning the Federation of Indian Chambers of Commerce and Industry's FICCI Frames 2016 hosted Best Animated Frames (BAF) for special critics on new entry in animation for <span class="bold-p">“Chhoti Anandi”</span> in GEC (General Entertainment Channel)</p>
                </div>
                
            </div>
        </section>

        <div class="team-section">
            <div class="container">
                <div class="head">Team</div>
                <div class="team-row">
                    <div class="team-box">
                        <div class="people-patch"><img src="dist/img/hopemotion/people-patch.png" class="img-responsive" alt=""></div>
                        <div class="people-desc">
                            <div class="people-img"><img src="dist/img/anish.png" class="img-responsive"></div>
                            <div class="people-name">Anish Patel</div>
                            <div class="people-desig">Business Head <br/> Content Development</div>
                            <p>A graduate from the University of California (Film & Digital Media Major), is a filmmaker, writer, animator and editor who has worked on various televisions shows and films as writer, creative director and show runner. He's also worked on various international animated projects for studios in North America over past 6 years.</p>
                        </div>
                    </div>
                    <div class="team-box">
                        <div class="people-patch"><img src="dist/img/hopemotion/people-patch.png" class="img-responsive" alt=""></div>
                        <div class="people-desc">
                            <div class="people-img"><img src="dist/img/neel.png" class="img-responsive">
                            </div>
                            <div class="people-name">Neel Lukkani</div>
                            <div class="people-desig">Technical Director</div>
                            <p>An inventive thinker and a technical wizard that keeps a toon-tank pumping and alive. Neel brings with himself 15 years of diversified experience from traditional animation, game programming to mastering the trends and bends of digital 2D animation, VFX, CG and Gaming.</p>
                        </div>
                    </div>
                </div>
                <div class="team-row">
                    <div class="team-box">
                        <div class="people-patch"><img src="dist/img/hopemotion/people-patch.png" class="img-responsive" alt=""></div>
                        <div class="people-desc">
                            <div class="people-img"><img src="dist/img/sikha.png" class="img-responsive">
                            </div>
                            <div class="people-name">Shikha Vij</div>
                            <div class="people-desig">Creative Director</div>
                            <p>With 15 years of experience in content creation, Shikha plays a leadership role to develop and manage content and has set up more than 20 shows for popular Indian GEC & digital platform. She collaborates with the team of writers to develop content that is engaging and entertaining.</p>
                        </div>
                    </div>
                    <div class="team-box">
                        <div class="people-patch"><img src="dist/img/hopemotion/people-patch.png" class="img-responsive" alt=""></div>
                        <div class="people-desc">
                            <div class="people-img"><img src="dist/img/carmen.png" class="img-responsive">
                            </div>
                            <div class="people-name">Carmen Zainabadi</div>
                            <div class="people-desig">Senior Writer</div>
                            <p>A yarn spinner,a tale teller and a witty narrator, Carmen writes for film, TV and web. With a Master in English Literature, she has worked on various film scripts as a screenwriter, story reviewer and story editor on numerous animated television shows including a CG-VFX driven fantasy film.</p>
                        </div>
                    </div>
                    <div class="team-box">
                        <div class="people-patch"><img src="dist/img/hopemotion/people-patch.png" class="img-responsive" alt=""></div>
                        <div class="people-desc">
                            <div class="people-img"><img src="dist/img/sunjanna.png" class="img-responsive">
                            </div>
                            <div class="people-name">Sunnjana. S</div>
                            <div class="people-desig">Associate Producer</div>
                            <p>Sunnjana has a Bachelor's degree with double majors in Business Finance & Real Estate with a minor in accounting from the University of San Diego, US. On behalf of the studio, she acts as a liaison between the producer, the technical and creative team.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="animation-sec">
            <div class="anim-img-poster">
                <img src="dist/img/hopemotion/animation-poster.jpg" class="img-responsive hidden-xs" alt="animation">
                <img src="dist/img/hopemotion/anim-poster-mobile.jpg" class="img-responsive visible-xs" alt="animation">
                <div class="content">
                    <div class="head">Join Our Team</div>
                    <div class="tv-history-btn">
                        <a href="career.php"><button type="button" class="btn sphereorigins-btn">know more</button></a>
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="quotes-bg">
            <div class="container">
                <div class="row-flex">
                <!-- <div class="hippo">
                    <img src="dist/img/hopemotion/quotes-hippo.png" class="img-responsive" alt="">
                </div> -->
                <div class="hippo">
                    <img src="dist/img/hopemotion/animate-hippo/animate-hippo.png" class="img-responsive hippo-img" alt="">
                    <img src="dist/img/hopemotion/animate-hippo/animate-hippo-shadow.png" class="img-responsive hippo-shadow-img" alt="">
                </div>
                    <div class="brief">
                        <div class="owl-carousel owl-theme quotes-slider">
                            <div class="item">
                                <h3 class="title">Transforming an idea into an immersive experience</h3>
                            </div>
                            <div class="item">
                                <h3 class="title">Creative thinkers with a strong passion to create exceptional content and mesmerizing visuals</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </section>
    <!--  end body content -->
    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>

    <script>
        $(function() {
            // Generic selector to be used anywhere
            $(".scroll-div").click(function(e) {
                // Get the href dynamically
                var destination = $(this).attr('href');
                // Prevent href=“#” link from changing the URL hash (optional)
                e.preventDefault();
                // Animate scroll to destination
                $('html, body').animate({
                scrollTop: $(destination).offset().top
                }, 1500);
            });
        });
    </script>

</body>

</html>
