<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <title>Sphereorigins | Regional Shows</title>
    <link rel="shortcut icon" type="image/x-icon" href="dist/img/favicon.png" />
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/loader.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->
    <section id="shows" class="shows">

        <div class="regional-banner-section">
            <div class="banner">
                <img src="dist/img/show/regional-banner.jpg" class="img-responsive center-block desktop visible-lg" alt="">
                <img src="dist/img/show/regional-tab.jpg" class="img-responsive center-block tablet visible-sm visible-md" alt="">
                <img src="dist/img/show/regional-mob.jpg" class="img-responsive center-block mob visible-xs" alt="">
            </div>
            <!-- <div class="container visible-lg">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="outer-valign">
                            <div class="inner-valign">
                                <div class="making-form-text hidden-xs" data-scroll>
                                    <img src="dist/img/show/show-text.png" class="" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>

        <div class="show-list-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Amar-Babin.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Amar Bapin<br class="hidden-xs"></h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Hamar-Sautan.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Hamar Sautan Hamar Saheli</h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Care-Koi-Na.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Care Kori Na</h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Chiro-Sathi.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Chiro Saathi</h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Meera-Mba-Kajer-Maye.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Meera Mba Kajer Maye<br class="hidden-xs"></h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Amar-Naam-Joyeta.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Amar Naam Joyeeta<br class="hidden-xs"></h2>
                    </div>
                    <div class="col-md-4 col-sm-4 show-list-desc">
                        <div class="back_img">
                            <img src="dist/img/show/Saninoy-Nibedan.jpg" alt="" class="img-responsive">
                        </div>
                        <h2>Sobinoy Nibedon</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="switch-to">
            <img src="dist/img/switch-to.png" class="img-responsive center-block" alt="">
        </div>

        <div class="our-works">
            <a class="work show" href="shows.php">
                <div class="text">Shows</div>
            </a>
            <a class="work telefilms" href="telefilms.php">
                <div class="text">Telefilms</div>
            </a>
            <a class="work animation" href="animation.php">
                <div class="text">Animations</div>
            </a>
        </div>

    </section>
    <!--  end body content -->
    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>

</body>

</html>
