<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <title>Sphereorigins | Audition</title>
    <link rel="shortcut icon" type="image/x-icon" href="dist/img/favicon.png" />
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/loader.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->
    <section id="auditions" class="auditions_wrap">

        <div class="container-fluid">
            <div class="row">

                <div id="banner">
                    <img src="dist/img/auditions/a_banner.jpg" class="img-responsive">
                    <!-- <h2 class="heading">AUDITIONS</h2> -->
                </div>

                <div class="container">
                    <div class="row formWrap">
                        <img src="dist/img/contact/start-pattern1.png" class="img-responsive left-pattern">
                        <div class="col-lg-offset-2 col-lg-8">

                            <h4>Think you can make the cut?<br>
                                Show off your potential.</h4>
                            <div class="row form-container">
                                <form id="auditionsForm" method="post">
                                    <div class="input-field col-xs-12 col-lg-6">
                                        <input id="first_name" name="first_name" type="text" class="validate" required="">
                                        <label for="first_name">First Name :</label>
                                    </div>
                                    <div class="input-field col-xs-12 col-lg-6">
                                        <input id="last_name" name="last_name" type="text" class="validate" required="">
                                        <label for="last_name">Last Name :</label>
                                    </div>
                                    <div class="input-field col-xs-12 col-lg-6">
                                        <input id="email" name="email" type="email" class="validate" required="">
                                        <label for="email">Your E-mail ID :</label>
                                    </div>
                                    <div class="input-field col-xs-12 col-lg-6">
                                        <input id="phone_no" name="phone_no" type="text" class="validate" required="">
                                        <label for="phone_no">Contact Number :</label>
                                    </div>
                                    <div class="file-field input-field col-xs-12">
                                        <div class="btn">
                                            <span>Browse</span>
                                            <input type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" name="file1" type="text" placeholder="Choose a file…" required="">
                                            <h6>Attach Resume | Max file size 500kb | Word format or PDF</h6>
                                        </div>
                                    </div>
                                    <div class="file-field input-field col-xs-12">
                                        <div class="btn">
                                            <span>Browse</span>
                                            <input type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" name="file2" type="text" placeholder="Choose a file…" required="">
                                            <h6>Maximum 4 images | Max file size 500kb each | To select more than 1 image, press "CTRL" & upload.</h6>
                                        </div>
                                    </div>
                                    <div class="input-field col-xs-12">
                                        <input id="downloadable" name="downloadable" type="text" class="validate">
                                        <label for="downloadable">Downloadable Link</label>
                                        <h6>Google drive link with share access or a wetransfer link or you could also share a Downloadable Link.</h6>
                                    </div>
                                    <div class="input-field col-xs-12">
                                        <input id="comment" name="comment" type="text" class="validate" required="">
                                        <label for="comment">Comment</label>
                                        <h5>Character Limit 400</h5>
                                    </div>
                                    <div class="col-xs-12">
                                        <button type="submit" value="submit" class="btn submit-btn">submit</button>
                                    </div>
                                </form>
                                <img src="dist/img/contact/start-pattern2.png" class="img-responsive right-pattern">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--  end body content -->
    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>
</body>

</html>
