<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <title>Sphereorigins | Home</title>
    <link rel="shortcut icon" type="image/x-icon" href="dist/img/favicon.png" />
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <?php include("includes/browser_upgrade.html") ?>

    <?php include("includes/loader.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->
    <section id="index" class="index">



        <div class="video-banner">
            <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop" class="hidden-xs">
                <source src="dist/img/homepage-desktop.mp4" type="video/mp4">
            </video>
            <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop" class="visible-xs">
                <source src="dist/img/homepage-mobile.mp4" type="video/mp4">
            </video>
            <!-- <img src="dist/img/show/show-banner.jpg" class="img-responsive" alt=""> -->
            <div class="scroll-div" href=".go-to-scroll">
                <p><span></span>Scroll Down</p>
            </div>
        </div>

        <div class="tv-history-section go-to-scroll">
            <div class="container">
                <div class="row div-overflow tv-history-flex">
                    <div class="col-md-5 col-sm-5 col-lg-5">
                        <div class="tv-history-img">
                            <img src="dist/img/making-tv-history.png" alt="" class="img-responsive history-couple-img">
                            <img src="dist/img/grey-patch.png" alt="" class="img-responsive grey-patch-img" data-scroll>
                            <div class="square-forming">
                                <span class="square-border form-border"></span>
                                <span class="square-border form-border"></span>
                                <span class="square-border form-border"></span>
                                <span class="square-border form-border"></span>
                            </div>
                        </div>
                        <!-- <img src="dist/img/tv-history.png" alt="" class="img-responsive"> -->
                    </div>
                    <div class="col-lg-6 col-lg-offset-1 col-md-7 col-sm-7 ipad-text-center">
                        <div class="tv-history-text">
                            <div class="making-form-text" data-scroll>
                                <img src="dist/img/making.png" class="making-text" alt="">
                            </div>
                            <img src="dist/img/tv-history-head.png" alt="" class="img-responsive down yellow-img-font-mob" data-scroll>
                            <img src="dist/img/zig-zag.png" alt="" class="img-responsive appear" data-scroll>
                            <p class="appear" data-scroll>From our founding in 2002 to the present moment, we’ve always had one goal: to move our viewers like never before.</p>
                        </div>
                    </div>
                </div>

                <div class="row tv-history-grey-bg" data-scroll>
                    <div class="col-md-4">
                        <div class="tv-history-block">
                            <h2>A Limca Record-Breaking Sphereorigins Original</h2>
                            <p>Balika Vadhu, our phenomenal television drama on child marriage, aired from 21 July 2008 to 31 July 2016, completing 2,245 episodes.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="tv-history-block">
                            <h2>Indian<br class="hidden-sm hidden-xs"> Television Dominance</h2>
                            <p>Sphereorigins has produced over 6500 hours of original broadcast television content.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="tv-history-block">
                            <h2>An Award-Winning Entry into Animation</h2>
                            <p>Chhoti Anandi, our animation creation, won a FICCI BAF award in 2016.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 tv-history-btn">
                        <a href="shows.php"><button type="button" class="btn sphereorigins-btn">know more</button></a>
                    </div>
                </div>

            </div>
        </div>

        <div class="conquering-animation-space-section">
            <div class="container">
                <div class="row div-overflow">
                    <div class="col-lg-4 col-lg-offset-1 col-md-5 col-sm-5 no-padd">
                        <img src="dist/img/girl-horse.png" alt="" class="img-responsive girl-horse-img">
                        <img src="dist/img/star-grey-patch.png" alt="" class="img-responsive star-grey-patch-img" data-scroll>
                    </div>
                    <div class="col-md-7 col-sm-7 col-lg-7 no-padd">
                        <div class="hopmotion-animation-text">
                            <div class="making-form-text" data-scroll>
                                <img src="dist/img/conquering.png" class="conquering-text" alt="">
                            </div>
                            <img src="dist/img/animation.png" alt="" class="img-responsive down yellow-img-font-mob" data-scroll>
                            <!-- <p class="bold-para appear" data-scroll>In late 2015, we took a step that absolutely transformed us:</p> -->
                            <br/>
                            <p class="appear" data-scroll>A division of Sphereorigins, Hopmotion is a creatively driven full-service animation studio that brings worlds to life through animations that excel. Quality animation for domestic and international markets is the goal; through passion is how we achieve it.</p>

                            <div class="row">
                                <div class="col-md-12 tv-history-btn">
                                    <a href="hopmotion.php"><button type="button" class="btn sphereorigins-btn">know more</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="indian-entertaiment-home-section">
            <div class="container entertainment-bg">
                <div class="row">
                    <div class="col-md-12 indian-entertaiment-text">
                        <h3 class="">
                            <div class="making-form-text" data-scroll>
                                <img src="dist/img/entertainment.png" class="entertainment-img" alt="">
                            </div>
                            <div class="down" data-scroll>Most Unique Home</div>
                        </h3>
                        <p class="appear" data-scroll>From the story of a child bride in Rajasthan to a narrative of Mumbai’s 26/11 terrorist attacks and an animated series about the innocence of childhood, we paint the most engrossing pictures of the real world, across formats and genres.</p>
                    </div>
                </div>
            </div>
            <div class="container entertainment-slider">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <select name="" id="select-tab-mob" class="visible-xs">
                            <option value="#tab1">Television</option>
                            <option value="#tab2">Telefilms</option>
                            <option value="#tab3">Regional</option>
                            <option value="#tab4">Animation</option>
                            <option value="#tab5">Flims</option>
                            <option value="#tab6">Web Series</option>
                        </select>
                        <ul class="nav nav-tabs hidden-xs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Television</a>
                            </li>
                            <li>
                                <a href="#tab2" data-toggle="tab">Telefilms</a>
                            </li>
                            <li>
                                <a href="#tab3" data-toggle="tab">Regional</a>
                            </li>

                            <li>
                                <a href="#tab4" data-toggle="tab">Animation</a>
                            </li>

                            <li>
                                <a href="#tab5" data-toggle="tab">Flims</a>
                            </li>
                            <li>
                                <a href="#tab6" data-toggle="tab">Web Series</a>
                            </li>
                        </ul>

                        <div class="tab-content up" data-scroll>
                            <div class="tab-pane active" id="tab1">
                                <div class="owl-carousel owl-theme indian-entertaiment-slider">
                                    <div class="item">
                                        <!--                                        <div class="back_img">-->
                                        <img src="dist/img/show/Balika-Vadhu.jpg" alt="" class="img-responsive">
                                        <!--                                        </div>-->
                                        <h4>Balika Vadhu</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Peshwa-Bajirao.jpg" alt="" class="img-responsive">
                                        <h4>Peshwa Bajirao</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Internet-wala-Love.jpg" alt="" class="img-responsive">
                                        <h4>Internet wala Love</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Gangaa.jpg" alt="" class="img-responsive">
                                        <h4>Gangaa</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Ek-tha-Raja-Ek-thi-Rani.jpg" alt="" class="img-responsive">
                                        <h4>Ek tha Raaja Ek thi Raani</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Mere-Angane-Main.jpg" alt="" class="img-responsive">
                                        <h4>Mere Angne Mein</h4>
                                    </div>
                                    <!-- <div class="item">
                                        <img src="dist/img/show/Choti-Anandi.jpg" alt="" class="img-responsive">
                                        <h4>Choti Anandi</h4>
                                    </div> -->
                                    <div class="item">
                                        <img src="dist/img/show/Piya-Rangrezz.jpg" alt="" class="img-responsive">
                                        <h4>Piya Rangrezz</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Saraswati-Chandra.jpg" alt="" class="img-responsive">
                                        <h4>Saraswati Chandra</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab2">
                                <div class="owl-carousel owl-theme indian-entertaiment-slider">
                                    <div class="item">
                                        <img src="dist/img/show/Barasat.jpg" alt="" class="img-responsive">
                                        <h4>Barsaath</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Chatpat-Jhatpat.jpg" alt="" class="img-responsive">
                                        <h4>Chatpat Jhatpat</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/un-hazzaro.jpg" alt="" class="img-responsive">
                                        <h4>Un Hazaaron Ke Naam</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/phir-se.jpg" alt="" class="img-responsive">
                                        <h4>Phir se</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/gol-magol.jpg" alt="" class="img-responsive">
                                        <h4>Gol Mol Gongol</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab3">
                                <div class="owl-carousel owl-theme indian-entertaiment-slider">
                                    <div class="item">
                                        <img src="dist/img/show/Amar-Babin.jpg" alt="" class="img-responsive">
                                        <h4>Amar Bapin</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Hamar-Sautan.jpg" alt="" class="img-responsive">
                                        <h4>Hamar Sautan Hamar Saheli</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Care-Koi-Na.jpg" alt="" class="img-responsive">
                                        <h4>Care Kori Na</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Chiro-Sathi.jpg" alt="" class="img-responsive">
                                        <h4>Chiro Saathi</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Meera-Mba-Kajer-Maye.jpg" alt="" class="img-responsive">
                                        <h4>Meera Mba Kajer Maye</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Amar-Naam-Joyeta.jpg" alt="" class="img-responsive">
                                        <h4>Amar Naam Joyeeta</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Saninoy-Nibedan.jpg" alt="" class="img-responsive">
                                        <h4>Sobinoy Nibedon</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab4">
                                <div class="owl-carousel owl-theme indian-entertaiment-slider">
                                    <div class="item">
                                        <img src="dist/img/show/Ku-Ku-Me-me.jpg" alt="" class="img-responsive">
                                        <h4>KuKu Mey Mey</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Choti-Anandi.jpg" alt="" class="img-responsive">
                                        <h4>Chhoti Anandi</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/show/Kung-Fu-singh.jpg" alt="" class="img-responsive">
                                        <h4>Kung Fu Singh </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab5">
                                <div class="films-webseries">
                                    <h4 class="para">
                                        We’re gearing up to launch commercial films that will make you laugh, cry, scream, and everything in between. Stay tuned!
                                    </h4>
                                </div>

                                <!--<div class="owl-carousel owl-theme indian-entertaiment-slider">
                                    <div class="item">
                                        <img src="dist/img/balika-vadhu.png" alt="" class="img-responsive">
                                        <h4>Balika Vadhu</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/gangaa.png" alt="" class="img-responsive">
                                        <h4>Gangaa</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/peshwa.png" alt="" class="img-responsive">
                                        <h4>Peshwa Bajirao</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/balika-vadhu.png" alt="" class="img-responsive">
                                        <h4>Balika Vadhu</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/gangaa.png" alt="" class="img-responsive">
                                        <h4>Gangaa</h4>
                                    </div>
                                    <div class="item">
                                        <img src="dist/img/peshwa.png" alt="" class="img-responsive">
                                        <h4>Peshwa Bajirao</h4>
                                    </div>
                                </div>-->
                            </div>
                            <div class="tab-pane" id="tab6">
                                <!-- <div class="films-webseries">
                                    <h4 class="para">
                                        We’re gearing up to launch web series that will make you laugh, cry, scream, and everything in between. Stay tuned!
                                    </h4>
                                </div> -->
                                <div class="owl-carousel owl-theme indian-entertaiment-slider">
                                    <div class="item">
                                        <img src="dist/img/show/Ku-Ku-Me-me.jpg" alt="" class="img-responsive">
                                        <h4>KuKu Mey Mey</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="achievements-section">
            <div class="container entertainment-bg">
                <div class="row">
                    <div class="col-md-12 indian-entertaiment-text">
                        <h3 class="">
                            <div class="making-form-text text-center" data-scroll>
                                <img src="dist/img/achievements.png" class="entertainment-img" alt="">
                            </div>
                        </h3>
                        <!-- <p class="appear" data-scroll>From the story of a child bride in Rajasthan to a narrative of Mumbai’s 26/11 terrorist attacks and an animated series about the innocence of childhood, we paint the most engrossing pictures of the real world, across formats and genres.</p> -->
                    </div>
                </div>
            
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="owl-carousel owl-theme achievements-slider">
                            <div class="item">
                                <div class="row-flex">
                                    <div class="achieve-img"><img src="dist/img/achievement-balikavadu.jpg" class="img-responsive" alt=""></div>
                                    <div class="achieve-brief">
                                        <h3 class="title">Balika Vadhu</h3>
                                        <p class="desc">Balika Vadhu was the longest daily Hindi soap aired on the launch of the channel Colors. Received several accolades for the different genre and also for the social message given in each episode. Balika Vadhu started a revolution for the child brides in Rajasthan. Many who were inspired, were bold enough to get out of such marriages and move on. On air from July 2008 to July 2016. Received many awards for the same and also mentioned in the Limca Book of Records.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row-flex">
                                    <div class="achieve-img"><img src="dist/img/achievement-saat-phere.jpg" class="img-responsive" alt=""></div>
                                    <div class="achieve-brief">
                                        <h3 class="title">Saath Phere</h3>
                                        <p class="desc">Saath Phere was one of the first shows that told a story of discirmination of girls with a dark skin. The show did extremely well world-wide and was a break-through for channel Zee.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row-flex">
                                    <div class="achieve-img"><img src="dist/img/achievement-mere-angne.jpg" class="img-responsive" alt=""></div>
                                    <div class="achieve-brief">
                                        <h3 class="title">Mere Angne Mein</h3>
                                        <p class="desc">Mere Angne Mein was the <span class="bold">first daily (Monday to Sunday) 1 hour Hindi soap</span> to be aired and a first production to use a Multi-Camera Set up.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row-flex">
                                    <div class="achieve-img"><img src="dist/img/achievement-silsila.jpg" class="img-responsive" alt=""></div>
                                    <div class="achieve-brief">
                                        <h3 class="title">Silsila Badalte Rishton Ka – <br/> Season 2</h3>
                                        <p class="desc">Silsila Badalte Rishton Ka – Season 2, was the first digital daily soap aired on Voot. It has a great fan following among the younger generation.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row-flex">
                                    <div class="achieve-img"><img src="dist/img/achievement-silsila2.jpg" class="img-responsive" alt=""></div>
                                    <div class="achieve-brief">
                                        <h3 class="title">Silsila Badalte Rishton Ka – <br/> Season 2</h3>
                                        <p class="desc">Silsila Badalte Rishton Ka – Season 2, was the first digital daily soap aired on Voot. In the span of 30 days it had more than 100 million views with 10+ million viewers.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row-flex">
                                    <div class="achieve-img"><img src="dist/img/rajkumar-aryan.jpg" class="img-responsive" alt=""></div>
                                    <div class="achieve-brief">
                                        <h3 class="title">Rajkumar Aryan</h3>
                                        <p class="desc">'Rajkumar Aryan' Sphereorigins first tyrst with Fantasy Genre launched Yami Gautam to TV screens.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row-flex">
                                    <div class="achieve-img"><img src="dist/img/saathire-parayadhan.jpg" class="img-responsive" alt=""></div>
                                    <div class="achieve-brief">
                                        <h3 class="title">Saathi Re / Paraaya Dhan</h3>
                                        <p class="desc">10th October 2006, was a proud day for Sphereorigins. Two Daily Series, 'Saathi Re' & 'Paraaya Dhan' launched simultaneously on Star One, creating a milestone in the history of GEC's.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="meet-team-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 meet-team-text">
                        <h3 class="">
                            <div class="making-form-text" data-scroll>
                                <img src="dist/img/meet-team-head.png" alt="" class="meet-img">
                            </div>
                            <img src="dist/img/sphereorigins.png" alt="" class="img-responsive center-block down yellow-img-font-mob" data-scroll>
                        </h3>
                        <img src="dist/img/zig-zag.png" alt="" class="img-responsive center-block down" data-scroll>
                        <p class="appear" data-scroll>Sphereorigins Multivision Private Limited was Founded by Sunjoy Wadhwa & Co-Founded by Comall Sunjoy W. Thanks to the pioneering efforts, today, we are an independent Indian entertainment brand that has multiple awards and even television records to its name.</p>
                        <div class="tv-history-btn hide">
                            <button type="button" class="btn sphereorigins-btn">Discover Our Story</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-1 col-sm-6">
                        <img src="dist/img/team.png" alt="" class="img-responsive team-img-ipad">
                        <div class="people-name">Sunjoy Waddhwa</div>
                        <div class="people-desig">Chairman & Managing Director</div>
                    </div>
                    <div class="col-md-4 col-md-offset-1 col-sm-6">
                        <img src="dist/img/comall.png" alt="" class="img-responsive team-img-ipad">
                        <div class="people-name">Comall Sunjoy W</div>
                        <div class="people-desig">Director</div>
                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="awards-winners-newsmakers">
            <div class="meet-team-star">
                <img src="dist/img/star.png" alt="" class="img-responsive">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 awards-winners-text">
                        <h3 class="">
                            <div class="making-form-text" data-scroll>
                                <img src="dist/img/award-winner-head.png" class="award-winner-head" alt="">
                            </div>
                            <div class="down" data-scroll>& Newsmakers</div>
                        </h3>
                        <p class="appear" data-scroll>Fortunately for us, we boast an award-winning<br>team and a newsmaking portfolio</p>
                    </div>
                    <div class="col-md-5 col-md-offset-1 col-sm-6 up" data-scroll>
                        <div class="awards-winners-blocks">
                            <img src="dist/img/a-w1.jpg" alt="" class="img-responsive">
                            <p>Sphereorigins names HopMotion as their animation division</p>
                            <button type="button" class="btn sphereorigins-btn">Read more <i class="fas fa-arrow-right"></i></button>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-6 up" data-scroll>
                        <div class="awards-winners-blocks">
                            <img src="dist/img/a-w2.jpg" alt="" class="img-responsive">
                            <p>Best Writers (Balika Vadhu)<br>Star Guild Awards 2014</p>
                            <button type="button" class="btn sphereorigins-btn">Read more <i class="fas fa-arrow-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="meet-team-star2">
                <img src="dist/img/star2.png" alt="" class="img-responsive">
            </div>
        </div> -->

        <div class="contact-section">
            <div class="container div-overflow">
                <div class="row">
                    <div class="col-md-7 contact-address-section">
                        <img src="dist/img/post-office.png" alt="" class="img-responsive post-office-img up">
                        <div class="contact-address-text">
                            <h3 class="">
                                <div class="making-form-text" data-scroll>
                                    <img src="dist/img/get-in-head.png" class="get-in-head" alt="">
                                </div>
                                <img src="dist/img/touch.png" alt="" class="img-responsive down yellow-img-font-mob" data-scroll>
                            </h3>
                            <h4>Registered Office</h4>
                            <p>
                                Sphereorigins Multivision Private Limited<br>
                                401, Nanak Chambers,<br>
                                Opposite Fun Republic,<br>
                                Off New Link Road,<br>
                                Andheri (West),<br>
                                Mumbai – 400053, India
                            </p>
                            <p>
                                <a href="tel:02240532000">Tel: +91 22 4053 2000</a>
                            </p>
                            <!-- <p>
                                <a href="tel:02240532001">Fax: +91 22 4053 2001</a>
                            </p> -->
                        </div>
                    </div>
                    <div class="col-md-5">
                        <img src="dist/img/map.png" alt="" class="img-responsive appear" data-scroll>
                    </div>
                </div>
                <div class="contact-yellow-bg-enquires up" data-scroll>
                    <div class="for-box">
                        <div class="for-content">
                            <div class="image-icon"><img src="dist/img/general-queries.png" class="img-responsive" alt=""></div>
                            <p>For any general queries email us on <br /> <a href="mailto:info@sphereorigins.com">info@sphereorigins.com</a></p>
                        </div>
                    </div>
                    <div class="for-box">
                        <div class="for-content">
                            <div class="image-icon"><img src="dist/img/casting-icon.png" class="img-responsive" alt=""></div>
                            <p>For casting related queries email us on <br /> <a href="mailto:casting@sphereorigins.com">casting@sphereorigins.com</a></p>
                        </div>
                    </div>
                    <div class="for-box">
                        <div class="for-content">
                            <div class="image-icon"><img src="dist/img/animation-icon.png" class="img-responsive" alt=""></div>
                            <p>For animation related queries email us on <br /> <a href="mailto:info@hopmotion.com">info@hopmotion.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--  end body content -->
    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>

    <script>
        // square forming in viewport starts
        $(window).scroll(function () {
            var section_top = $(window).scrollTop();
            // console.log(section_top);
            var section_offset = $(".tv-history-section").offset().top;
            // console.log(section_offset);
            if (section_top >= section_offset - winH / 2) {
                $(".square-forming .square-border:nth-child(1)").addClass("square-border-top");
                $(".square-forming .square-border:nth-child(2)").addClass("square-border-left");
                $(".square-forming .square-border:nth-child(3)").addClass("square-border-bottom");
                $(".square-forming .square-border:nth-child(4)").addClass("square-border-right");
            } else {
                //   	$(".square-forming .square-border:nth-child(1)").removeClass("square-border-top");
                // $(".square-forming .square-border:nth-child(2)").removeClass("square-border-left");
                // $(".square-forming .square-border:nth-child(3)").removeClass("square-border-bottom");
                // $(".square-forming .square-border:nth-child(4)").removeClass("square-border-right");
            }
        });
        // square forming in viewport ends
        
        $(function() {
            // Generic selector to be used anywhere
            $(".scroll-div").click(function(e) {
                // Get the href dynamically
                var destination = $(this).attr('href');
                // Prevent href=“#” link from changing the URL hash (optional)
                e.preventDefault();
                // Animate scroll to destination
                $('html, body').animate({
                scrollTop: $(destination).offset().top
                }, 1500);
            });
        });
    </script>

</body>

</html>
