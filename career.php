<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <title>Careers | Hopmotion</title>
    <link rel="shortcut icon" type="image/x-icon" href="dist/img/hop-favicon.png" />
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/loader.html") ?>
    <?php include("includes/hopemotion-header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->
    <section id="career" class="career">
        <div class="career-banner-section">
            <img src="dist/img/career/career-banner.png" class="img-responsive hidden-xs">
            <img src="dist/img/career/careers-mobile.jpg" class="img-responsive visible-xs">
        </div>
        <div class="lifeat">
            <div class="container">
                <div class="lifeat-content">
                    <div class="lifeat-content-image">
                        <div class="align-img-vertical"><img src="dist/img/career/life-at.png"
                                class="img-responsive"><img src="dist/img/zig-zag.png" class="img-responsive"></div>
                        <div class="align-img-vertical"><img src="dist/img/career/hopmotion.png" class="img-responsive">
                        </div>
                    </div>
                    <!-- <p>Hopmotion Animation, a division of Sphereorigins Multivision
                        Private Limited is a creatively driven full service animation studio
                        focused on original IP and international service work for television
                        and digital networks. We foster a philosophy that brings together artists and technicians in a
                        creative, innovate yet collaborative atmosphere. We apply our advanced skills to both 2D and 3D animation programmes
                        by innovating the art of storytelling techniques and overseeing the entire
                        production. </p> -->
                        <h5>Join a Team of Creative Innovators</h5>
                        <p class="text-center">Wondering why you should join Hopmotion Animation?</p>
                        <p class="text-center">Wondering what makes us a team worth being a part of?</p>
                        <p class="text-center">Wondering how you can contribute? </p> <br/>
                        <p>Team Hopmotion is a potpourri of overactive minds that thrive on any and every opportunity to create art. Joining us will mean joining a dedicated team that can handle every stage of the making of a 2D or 3D animated series, including direction, design, storyboards, sets, animation, compositing, rendering, editing, and post-production. We innovate at every step to reach new animation highs.</p> <br/>
                        <p>Hopmotion Animation is a studio that follows a completely digital pipeline and is powered by Toon Boom Harmony animation software and Storyboard Pro. Our new Hyderabad studio also has a classroom wherein we train youngsters who are looking to kick-start their careers in animation. Our on-the-job training program puts together creative minds and strong teams: a combination of knowledgeable experienced professionals and dynamic youth.</p> <br/>
                        <p>Join us and be a part of an award-winning mix of professionals.</p>
                </div>
            </div>
        </div>

        <section class="job-openings">
            <div class="job-openings-text img-mid">
                <div class="align-img-vertical black-img"><img src="dist/img/career/job.png" class="img-responsive"></div>
                <div class="align-img-vertical yellow-img"><img src="dist/img/career/openings.png" class="img-responsive"></div>
                <div class="underline"><img src="dist/img/zig-zag.png" class="img-responsive"></div>
            </div>
            <div class="job-roles">
                <div class="spacing-horizontal">
                    <div class="row d-flex">
                        <div class="col-md-4 col-sm-12 d-flex">
                            <div class="bg-white">
                                <h4>Character Designer & Concept Artist</h4>
                                <h5>Location:</h5>
                                <p>Hyderabad & Mumbai</p>
                                <h5>Job Description :</h5>
                                <p>HopMotion is looking for a senior character designer and concept artist who is
                                    passionate about 2D animation. The candidate will be in charge of ideating and
                                    creating character designs and concept art for ongoing and upcoming animated
                                    shows.<br><br>
                                    Please apply to <a class="link"
                                        href="mailto:careers@hopmotion.com">careers@hopmotion.com</a> and mention the
                                    position you are applying for in the Subject of the mail. To be considered for the
                                    position, please include a link to your portfolio.</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 d-flex">
                            <div class="bg-white">
                                <h4>Storyboard<br>Artist</h4>
                                <h5>Location:</h5>
                                <p>Hyderabad & Mumbai</p>
                                <h5>Job Description :</h5>
                                <p>Candidate should have excellent Story telling & Composition skills. Knowledge in Toon
                                    Boom Storyboard is advantage. Beginners with strong art background and animation
                                    training can also apply.<br><br>
                                    Please apply to <a class="link"
                                        href="mailto:careers@hopmotion.com">careers@hopmotion.com</a> and mention the
                                    position you are applying for in the Subject of the mail. To be considered for the
                                    position, please include a link to your showreel.</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 d-flex">
                            <div class="bg-white">
                                <h4>2D Digital<br>Animators</h4>
                                <h5>Location:</h5>
                                <p>Hyderabad & Mumbai</p>
                                <h5>Job Description :</h5>
                                <p>Candidate should possess excellent traditional drawing skills and observational
                                    skills. Knowledge in Toon Boom Harmony is an advantage. Software training will be
                                    provided. Beginners with a strong art background and animation training can also
                                    apply.<br><br>
                                    Please apply to <a class="link"
                                        href="mailto:careers@hopmotion.com">careers@hopmotion.com</a> and mention the
                                    position you are applying for in the Subject of the mail. To be considered for the
                                    position, please include a link to your portfolio/ showreel</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <!--  end body content -->
    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>
</body>

</html>