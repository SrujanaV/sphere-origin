<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->

<head>
    <link rel="shortcut icon" type="image/x-icon" href="dist/img/favicon.png" />
    <?php include("includes/include_css.html") ?>
</head>

<body>
<?php include("includes/browser_upgrade.html") ?>
<?php include("includes/loader.html") ?>
<?php include("includes/header.html") ?>
<?php include("includes/sidebar.html") ?>
<?php include("includes/sticky-social.html") ?>
<!--  insert body content  -->
<section id="blog-inside" class="blog-inside">

    <div class="blog-inside-banner-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 blog-inside-img">
                    <img src="dist/img/blog-inside/blog-inside-banner.jpg" alt="" class="img-responsive">
                </div>
                <div class="col-md-10 col-md-offset-1 blog-inside-desc">
                    <h2>January 9, 2018</h2>
                    <h1>HOPMOTION EXPANDS IN HYDERABAD</h1>
                    <p>Hoppo has a new home and our young team invited the New year with great aplomb in its new office in the city of Hyderabad. The new office with its 3000 sq feet floor space is located close to the Hi tech city area and Hoppo couldn’t be more proud. With the creative arm still in Mumbai, Hopmotion growing needs will be met with the talent pool of the city.</p>

                    <p>Hopmotion is completely fuelled to focus on creating original content and catering to the demand of high quality animation service work with its new facility powered by Toon Boom Harmony.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="blog-inside-slider-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="owl-carousel owl-theme blog-inside-slider">
                        <div class="item">
                            <img src="dist/img/blog-inside/blog-inside-banner.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="item">
                            <img src="dist/img/blog-inside/blog-inside-banner.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="item">
                            <img src="dist/img/blog-inside/blog-inside-banner.jpg" alt="" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!--  end body content -->
<?php include("includes/footer.html") ?>
<?php include("includes/include_js.html") ?>

</body>
</html>
