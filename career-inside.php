<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <link rel="shortcut icon" type="image/x-icon" href="dist/img/favicon.png" />
    <?php include("includes/include_css.html") ?>
</head>

<body>
    <?php include("includes/browser_upgrade.html") ?>
    <?php include("includes/loader.html") ?>
    <?php include("includes/header.html") ?>
    <?php include("includes/sidebar.html") ?>
    <!--  insert body content  -->
    <section id="career-inside" class="career">
        <div class="container-fluid">
            <div class="row">
               <img src="dist/img/career-inside/star-bg.png" class="img-responsive dot-bg">
                <div class="col-md-12">
                    <!-- <div class="row"> -->

                        <section class="banner-heading">
                            <div class="spacing-horizontal">
                                <div class="heading">
                                    <h2 class="profile-name">Anish patel</h2>
                                    <h4 class="designation">Business Head<br/>Content Development</h4>
                                    <img src="dist/img/career-inside/profile-icon.png" class="img-responsive profile">
                                </div>  
                            </div>
                        </section>


                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <section class="details">
                                        <p>A self-induced trance atop a steep mount of a dormant volcano is all it took Anish Patel to conceive the idea for the toon-tank that is HopMotion. After graduating from Film school in California, Anish wandered the global media landscape. Rumor has it that he broke bread with many giants from the film and TV universe around the world until he found his answer in the media wilderness of India. From here Anish built HopMotion block by block, from a team of 5 fierce and talented master strokesmen and women it has now grown to a team of 50 and counting. Honestly Anish would run Hopmotion from a tree house if he had to. As of now he is busy keeping a bird’s eye view on the many operations of Hopmotion. Living it, breathing and tooning away.</p>
                                </section>
                            </div>
                        </div>


                        <section class="people">
                            <div class="meet-our-people">
                                
                                <div class="meet-people-text">
                                    <div class="align-img-vertical black-img"><img src="dist/img/career-inside/other.png" class="img-responsive">
                                    </div>
                                    <div class="align-img-vertical yellow-img"><img src="dist/img/career-inside/profile.png" class="img-responsive"></div>
                                    <div><img src="dist/img/zig-zag.png" class="img-responsive"></div>
                                </div>
                            </div>
                            <div class="people-images">
                                <div class="spacing-horizontal">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6">
                                            <div class="people-desc">
                                                <img src="dist/img/career-inside/star.png" class="img-responsive star">
                                                <div class="people-img"><img src="dist/img/career/people1.png" class="img-responsive">
                                                </div>
                                                <div class="people-name">ANISH PATEL</div>
                                                <div class="people-desig">Business Head Content Development</div>
                                                <div class="social text-center"><a href="" target="_blank" class="mx-10"><i
                                                            class="fab fa-twitter"></i></a><a href="" target="_blank" class="mx-10"><i
                                                            class="fab fa-linkedin-in"></i></a><a href="" target="_blank"
                                                        class="mx-10"><i class="fab fa-instagram"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="people-desc">
                                                <img src="dist/img/career-inside/star.png" class="img-responsive star">
                                                <div class="people-img"><img src="dist/img/career/people2.png" class="img-responsive">
                                                </div>
                                                <div class="people-name">CARMEN ZAINABADI</div>
                                                <div class="people-desig">Business Head Content Development</div>
                                                <div class="social text-center"><a href="" target="_blank" class="mx-10"><i
                                                            class="fab fa-twitter"></i></a><a href="" target="_blank" class="mx-10"><i
                                                            class="fab fa-linkedin-in"></i></a><a href="" target="_blank"
                                                        class="mx-10"><i class="fab fa-instagram"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="people-desc">
                                                <img src="dist/img/career-inside/star.png" class="img-responsive star">
                                                <div class="people-img"><img src="dist/img/career/people3.png" class="img-responsive">
                                                </div>
                                                <div class="people-name">NEEL</div>
                                                <div class="people-desig">Technical Director</div>
                                                <div class="social text-center"><a href="" target="_blank" class="mx-10"><i
                                                            class="fab fa-twitter"></i></a><a href="" target="_blank" class="mx-10"><i
                                                            class="fab fa-linkedin-in"></i></a><a href="" target="_blank"
                                                        class="mx-10"><i class="fab fa-instagram"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="people-desc">
                                                <img src="dist/img/career-inside/star.png" class="img-responsive star">
                                                <div class="people-img"><img src="dist/img/career/people4.png" class="img-responsive">
                                                </div>
                                                <div class="people-name">ANISH PATEL</div>
                                                <div class="people-desig">Business Head Content Development</div>
                                                <div class="social text-center"><a href="" target="_blank" class="mx-10"><i
                                                            class="fab fa-twitter"></i></a><a href="" target="_blank" class="mx-10"><i
                                                            class="fab fa-linkedin-in"></i></a><a href="" target="_blank"
                                                        class="mx-10"><i class="fab fa-instagram"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                   <!-- </div>  -->
                </div>
            </div>
        </div>
        
        
        

        
    </section>
    <!--  end body content -->
    <?php include("includes/footer.html") ?>
    <?php include("includes/include_js.html") ?>
</body>

</html>