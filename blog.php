<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->

<head>
    <link rel="shortcut icon" type="image/x-icon" href="dist/img/favicon.png" />
    <?php include("includes/include_css.html") ?>
</head>

<body>
<?php include("includes/browser_upgrade.html") ?>
<?php include("includes/loader.html") ?>
<?php include("includes/header.html") ?>
<?php include("includes/sidebar.html") ?>
<!--  insert body content  -->
<section id="blog" class="blog">

    <div class="blog-banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <img src="dist/img/blog/blog-banner.png" alt="" class="img-responsive hidden-xs">
                    <img src="dist/img/blog/mobile-blog-banner.jpg" alt="" class="img-responsive visible-xs">
                </div>
            </div>
        </div>
    </div>

    <div class="blog-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 our-blog-text">
                    <img src="dist/img/blog/our-text.png" alt="" class="img-responsive our-text-img">
                    <img src="dist/img/blog/blog-text.png" alt="" class="img-responsive blog-text-img">
                    <img src="dist/img/blog/blog-head-zig-zag.png" alt="" class="img-responsive img-mid">
                    <img src="dist/img/blog/star-blog.png" alt="" class="img-responsive star-blog-img">
                </div>
            </div>

            <div class="select-row">
                <div class="category-select">
                    <select class="wide">
                        <option>All</option>
                        <option value="1">Shows</option>
                        <option value="2">Telefilms</option>
                        <option value="3">Regional</option>
                        <option value="4">Animations</option>
                    </select>
                </div>
                <div class="year-select">
                    <select class="wide">
                        <option>Latest</option>
                        <option value="1">2019</option>
                        <option value="2">2018</option>
                        <option value="3">2016</option>
                        <option value="4">2015</option>
                    </select>
                </div>
            </div>

            <!-- <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" href="#upcoming" role="tab" data-toggle="tab">Upcoming</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#past" role="tab" data-toggle="tab">Past</a>
                </li>
            </ul> -->

            <!-- Tab panes -->
            
                    <div class="row blog-same-height">
                        <div class="col-md-5 col-sm-12 col-xs-12">
                            <div class="left-block-blogs">
                                <img src="dist/img/blog/blog1.jpg" alt="" class="img-responsive">
                                <div class="left-block-blogs-text">
                                    <h1>HOPMOTION EXPANDS IN HYDERABAD</h1>
                                    <h2>January 9, 2018</h2>
                                    <p>Hoppo has a new home and our young team invited the New year with great aplomb in its new office</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-12 col-xs-12 right-block-blog" style="background-image: url('dist/img/blog/blog2.jpg');">
                            <div class="right-block-blogs-text">
                                <h1>ALL SMILES AT ORBIT LIVE ANIMATION EVENT – HOPMOTION WINS INDUSTRY HONORS 2017</h1>
                                <h2>February 2, 2017</h2>
                                <p>A proud moment for Founder and Creative Writer, Mr. Anish Patel, as HopMotion wins the Industry Honours at Orbit Live Animation Event, 2017.</p>
                            </div>
                        </div>
                    </div>

            <div class="row blog-yellow-section">
                <div class="col-md-12 yellow-bg">
                    <h1>WRITER TO ENTREPRENEUR – INTERVIEW WITH ANISH PATEL, FOUNDER OF HOPMOTION</h1>
                    <h2>November 29, 2016</h2>
                    <p>Hoppo is super happy and thrilled to be featured in the animation digital digest. It’s been three years now and our young animation studio’s focus remains on getting better each day. Here’s a link to the interview with our founder and CEO Anish Patel. It gives you a sneak peek into the workings of the Toon Tank that is HopMotion and what is in store for us on the road ahead.</p>
                </div>
            </div>

            <div class="below-blog-section">
                <div class="box">
                    <div class="img-div"><img src="dist/img/blog/blog3.jpg" class="img-responsive" alt=""></div>
                    <div class="blog-text">
                        <h1 class="headline">COLORS TV TO LAUNCH ‘CHHOTI ANANDI’</h1>
                        <h2 class="date">January 4, 2016</h2>
                        <p class="brief">Hoppo’s blue cheeks are flushed and pink with excitement. It’s been over a year now the Hopposapiens have been working</p>
                    </div>
                </div>
                <div class="box">
                    <div class="img-div"><img src="dist/img/blog/blog4.jpg" class="img-responsive" alt=""></div>
                    <div class="blog-text">
                        <h1 class="headline">KUNG FU SINGH – COMING SOON ON HOPMOTION.TV</h1>
                        <h2 class="date">November 13, 2014</h2>
                        <p class="brief">“Kung Fu Singh” is a hyperactive, six-year-old sardar who worships martial arts and lives and breathes it. But his love</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!--  end body content -->
<?php include("includes/footer.html") ?>
<?php include("includes/include_js.html") ?>

</body>
</html>
